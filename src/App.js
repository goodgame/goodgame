import React, {Component} from 'react';
import Footer from "./components/layout/Footer";
import {Container} from "reactstrap";
import Header from "./components/layout/Header";
import {BrowserRouter as Router} from "react-router-dom";
import Routes from "./components/routing/Routes";
import './App.css';

class App extends Component {
    render() {

        return (
            <Container style={{padding: "0px"}}>
                <Router>
                    <Header/>
                    <Routes/>
                    <Footer/>
                </Router>
            </Container>
        );
    };
}

export default App;
