import React, {Component} from "react";
import PropTypes from "prop-types";

import {
    Card as CardStrap,
    CardHeader as CardHeaderStrap,
    CardTitle as CardTitleStrap,
    CardBody as CardBodyStrap,
    CardText as CardTextStrap,
    CardFooter as CardFooterStrap
} from 'reactstrap';

import './Card.css';

class Card extends Component {
    static propTypes = {
        title: PropTypes.string,
        footer: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ])
    };

    render() {
        return (
            <CardStrap className="cardStrap">
                <CardHeaderStrap className="cardHeaderStrap">
                    <CardTitleStrap className="cardTitleStrap">{this.props.title}</CardTitleStrap>
                </CardHeaderStrap>
                <CardBodyStrap
                    className={this.props.footer === undefined || this.props.footer === '' ?
                                'cardBodyStrap bottomBorder' :
                                'cardBodyStrap'}
                >
                    <CardTextStrap className="cardTextStrap">{this.props.children}</CardTextStrap>
                </CardBodyStrap>
                {this.props.footer !== undefined && this.props.footer !== '' ?
                    (<CardFooterStrap className='cardFooterStrap bottomBorder'>
                        {this.props.footer}
                    </CardFooterStrap>) :
                    ''
                }
            </CardStrap>
        );
    }
}

export default Card;
