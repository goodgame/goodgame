import React, {Component} from 'react';
import {Link} from "react-router-dom";

class ReactstrapNavLink extends Component {
    render() {
        return (
            <Link className="nav-link" to={this.props.to}>{this.props.children}</Link>
        );
    }
}

export default ReactstrapNavLink;
