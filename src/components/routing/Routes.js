import React, {Component} from "react";
import {Route, Switch} from 'react-router-dom';

import Home from "../../pages/home/Home";
import ArticlesList from '../../pages/articles/ArticlesList';
import Partenaires from "../../pages/partenaires/Partenaires";

class Routes extends Component {
    render() {
        return (
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/articles" exact component={ArticlesList}/>
                <Route path="/partenaires" exact component={Partenaires}/>
            </Switch>
        );
    }
}

export default Routes;
