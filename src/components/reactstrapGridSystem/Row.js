import React from 'react';
import {Row as RowStrap} from 'reactstrap';

export const Row = (props) => (
    <RowStrap {...props} style={{padding: "0px"}}/>
);
