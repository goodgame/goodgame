import React from 'react';
import {Container as ContainerStrap} from 'reactstrap';

export const Container = (props) => (
    <ContainerStrap {...props} style={{padding: "0px"}}/>
);
