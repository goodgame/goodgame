import React from 'react';
import {Col as ColStrap} from 'reactstrap';

export const Col = (props) => (
    <ColStrap {...props} style={{padding: "0px"}}/>
);
