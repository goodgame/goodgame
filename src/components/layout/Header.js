import React, { Component } from 'react';
import {
    Collapse,
    Navbar,
    NavbarBrand,
    NavbarToggler,
    Nav,
    NavItem,
} from 'reactstrap';
import ReactstrapNavLink from "../routing/ReactstrapNavLink";
import './Header.css';
import {Row} from '../../components/reactstrapGridSystem/Row';
import {Col} from '../../components/reactstrapGridSystem/Col';

class Header extends Component {

    state = {collapse: null};

    constructor(props) {
        super(props);

        this.state = {collapse: true};
    }

    toggleNavbar = () => {
        this.setState({collapse: !this.state.collapse});
    };

    render() {

        return (
            <>
                <Row>
                    <Col className="cadreLogo">
                        <img
                            src={"./images/nom_et_slogan_goodgame.png"}
                            alt="Logo de l'association GoodGame"
                            style={{width: "70%"}}
                            className="logoEnTete"
                        />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Navbar color="dark" dark className="navBar" expand="md">
                            <NavbarBrand href="/">Accueil</NavbarBrand>
                            <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
                            <Collapse isOpen={!this.state.collapse} navbar>
                                <Nav navbar>
                                    <NavItem>
                                        <ReactstrapNavLink to="/articles">Articles</ReactstrapNavLink>
                                    </NavItem>
                                    <NavItem>
                                        <ReactstrapNavLink to="/galerie">Galerie</ReactstrapNavLink>
                                    </NavItem>
                                    <NavItem>
                                        <ReactstrapNavLink to="/evenements">Evenements</ReactstrapNavLink>
                                    </NavItem>
                                    <NavItem>
                                        <ReactstrapNavLink to="/contact">Contact</ReactstrapNavLink>
                                    </NavItem>
                                    <NavItem>
                                        <ReactstrapNavLink to="/partenaires">Partenaires</ReactstrapNavLink>
                                    </NavItem>
                                </Nav>
                            </Collapse>
                        </Navbar>
                    </Col>
                </Row>
            </>
        );
    }
}

export default Header;
