import React, { Component } from 'react';
import {Row, Col} from "reactstrap";
import './Footer.css';

/* Icons import */
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faFacebookSquare, faTwitterSquare, faYoutubeSquare, faTwitch } from '@fortawesome/free-brands-svg-icons';
import {Container} from "../reactstrapGridSystem/Container";

class Footer extends Component {
    render() {
        return (
            <Row className="cadreFooter">
                <Container>
                    <Row className="ligneFooter">
                        <Col style={{textAlign: "center"}}>
                            <a
                                href="https://www.facebook.com/GoodGameLacrouzette/"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <FontAwesomeIcon icon={faFacebookSquare} size="3x" className="iconeFooter"/>
                            </a>
                            <a
                                href="https://twitter.com/goodgamelan81"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <FontAwesomeIcon icon={faTwitterSquare} size="3x" className="iconeFooter"/>
                            </a>
                            <a
                                href="https://www.youtube.com/channel/UCtbfWU57MzOYHZM8sXV83Bg"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <FontAwesomeIcon icon={faYoutubeSquare} size="3x" className="iconeFooter"/>
                            </a>
                            <a
                                href="https://www.twitch.tv/goodgamelacrouzette"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <FontAwesomeIcon icon={faTwitch} size="3x" className="iconeFooter"/>
                            </a>
                        </Col>
                    </Row>
                    <Row className="ligneFooter">
                        <Col className="mentionsLegales">
                            Mentions légales
                        </Col>
                        <Col className="hautDePage">
                            Haut de la page
                        </Col>
                    </Row>
                </Container>
            </Row>
        );
    }
}

export default Footer;
