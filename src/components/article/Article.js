import React from "react";
import PropTypes from "prop-types";

/* -=-=-=-=-=-=-=- External libraries -=-=-=-=-=-=-=- */
import {Badge} from "reactstrap";
import ReactMarkdown from "react-markdown";

import Card from "../card/Card";

class Article extends React.Component {
    static propTypes = {
        article: PropTypes.objectOf(PropTypes.shape({
            title: PropTypes.string,
            text: PropTypes.string,
            date: PropTypes.string,
            tags: PropTypes.arrayOf(PropTypes.string)
        })).isRequired
    };

    articleFooter = (article) => {
        return article.date !== '' || (article.tags !== undefined && article.tags.length !== 0) ? (
            <>
                {article.date !== undefined ? <span>Publié le - {article.date}</span> : ''}

                <span
                style={{
                    display: 'float',
                    float: 'right'
                }}>
                    {article.tags !== undefined ? article.tags.map((tag, index) => (
                        <Badge
                            color='dark'
                            key={index}
                            style={{
                                marginLeft: '1px'
                            }}
                        >
                            {tag}
                        </Badge>
                    )) : ''}
                </span>
            </>
        ) : '';
    };

    render() {
        return this.props.article.date !== undefined && this.props.article.date !== "" &&
            this.props.article.tags !== undefined && this.props.article.tags.length !== 0 ?
        (
            <Card title={this.props.article.title} footer={this.articleFooter(this.props.article)}>
                <ReactMarkdown linkTarget="_blank">
                    {this.props.article.text}
                </ReactMarkdown>
            </Card>
        ) : (
                <Card title={this.props.article.title}>
                    <ReactMarkdown linkTarget="_blank">
                        {this.props.article.text}
                    </ReactMarkdown>
                </Card>
            );
    }
}

export default Article;