import React, {Component} from "react";

import {Row} from '../../components/reactstrapGridSystem/Row';
import {Col} from '../../components/reactstrapGridSystem/Col';
import Card from '../../components/card/Card';

import './Partenaires.css';

/* #TODO temporary while REST API is not set */
import listePartenaires from './liste_partenaires';

class Partenaires extends Component {
    render() {
        return (
            listePartenaires.map((partenaire, index) => (
                <Row key={index} className="partnerRow">
                    <Col>
                        <Card title={partenaire.nom}>
                            {   /** Affichage de l'adresse si elle est disponible */
                                partenaire.adresse.numeroDeRue === null &&
                                    partenaire.adresse.nomDeRue === null &&
                                    partenaire.adresse.ville === null &&
                                    partenaire.adresse.codePostal === null ?
                                    'Pas d\'adresse disponible' :
                                    `${
                                        partenaire.adresse.numeroDeRue !== null ? 
                                            partenaire.adresse.numeroDeRue : 
                                            ''
                                    }
                                    ${
                                        partenaire.adresse.nomDeRue !== null ?
                                            partenaire.adresse.nomDeRue :
                                            ''
                                    },
                                    ${                                    
                                        partenaire.adresse.ville !== null ?
                                            partenaire.adresse.ville :
                                            ''
                                    }
                                    ${
                                    partenaire.adresse.codePostal !== null ?
                                        partenaire.adresse.codePostal :
                                        ''
                                    }`
                        }
                        </Card>
                    </Col>
                </Row>
            ))
        );
    }
}

export default Partenaires;
