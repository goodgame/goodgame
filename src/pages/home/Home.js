import React, {Component} from 'react';

/* -=-=-=-=- External libraries -=-=-=-=- */
import {Button} from "reactstrap";

import './Home.css';

/* -=-=-=-=- Custom components -=-=-=-=- */
import {Col} from '../../components/reactstrapGridSystem/Col';
import ReactstrapNavLink from "../../components/routing/ReactstrapNavLink";
import Article from "../../components/article/Article";

import article from './article';
import {Row} from "../../components/reactstrapGridSystem/Row";

/**
 * #TODO temporary while REST API is not set
 * With the API, this page will call /api/article to get the last published article
 *
 * To display, or not, the button "Plus d'articles",
 * the page will call /api/articlesNumber and check if the return value is a number greater than 1
 */
class Home extends Component {
    render() {
        return(
            <>
                <Row>
                    <Col>
                        <Article
                            article={{
                                title: "Présentation",
                                text: "Ceci est le contenu du dernier article publié.\n" +
                                    "                            GoodGame est une association proposant des tournois de jeux vidéo en réseaux local (LAN).\n" +
                                    "                            Fondé en 2017 par une bande de potes suite a la pause de l'association NetExperience.\n" +
                                    "                            Nous vous proposons 3 LAN par année, une au printemps (avril), une en été (août) et une en hiver (décembre).\n" +
                                    "                            Vous trouverez sur ce site toutes les actualités et informations de l'association ainsi que la possibilité de s'inscrire aux prochains événements.\n" +
                                    "                            Nous vous souhaitons de faire des Good Game"
                            }}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Article article={article}/>
                    </Col>
                </Row>
                <Row>
                    <Col className='center'>
                        <ReactstrapNavLink to="/articles">
                            <Button className='btnPlusDarticles' outline color='secondary'>
                                Plus d'articles
                            </Button>
                        </ReactstrapNavLink>
                    </Col>
                </Row>
            </>
        );
    }
}

export default Home;
